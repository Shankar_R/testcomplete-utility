This Script extension controls the execution and generates the Awesome HTML report. And simple to implement.

 

To implement this, you need setup below things. I have attached sample project for this.

 

Download DataDrivenFramework.tcx file from attachments
Follow the instructions in the link to install the Script extension https://support.smartbear.com/testcomplete/docs/working-with/extending/script/installing-and-uninsta...
Create a function in anywhere in your project and do as below
	VB:
	
	Function InitExecution
	     'This is to initiate the execution
		adFramework.startExecution()
	End Function

	JavaScript/JScript:
	
	Function InitExecution
	     'This is to initiate the execution
		adFramework.startExecution()
	End Function
Usually in TC you will Log.Checkpoit or Log.Error functions in-order to make reports but here instead of that use below functions to do the same. It will create TC logs as well as HTML reports.
      //For adding a passed step
      adFramework.CreateTestStep(1,"Expected result","Actual result","Testdata used");

      //For adding failed step[by default you will see the screenshot in the HTML report]
      adFramework.CreateTestStep(0,"Expected result","Actual result","Testdata used");

      //For adding warning step
      adFramework.CreateTestStep(2,"Expected result","Actual result","Testdata used");

      //For adding warning step with screenshot
      adFramework.CreateTestStep(2,"Expected result","Actual result","Testdata used",true);

      //For adding done step [like clicking buttons, and some information messages]
      adFramework.CreateTestStep(3,"Expected result","Actual result","Testdata used");

      //For adding a Title
      adFramework.CreateTestStep("","Title","","",false,true);
In your scripts you need to get the from the Excel for each test case for that you can use below functions
//To get the test data from the excel sheet
adFramework.GetTestData("ColumnName");

//To get the environment from environment sheet
adFramework.GetEnvironmentData("ColumnName");
 

That's it you got your Data-driven framework. Happy automating!

Feel free ask your clarifications on this!